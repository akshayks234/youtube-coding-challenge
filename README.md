# Description
It is a simple application which allow user to search youtube videos based on the keyword enter.

## Prerequisites
```
Node - v10.20.1
Yarn - v1.13.0
```


## Set below environment variable to .env file to enable youtube api 

```
REACT_APP_YOUTUBE_API_KEY=${ADD_YOUTUBE_KEY_HERE}
```

## Run Dev Server:

```
1. yarn install
2. yarn start
```

## Run Tests:

```
yarn test
```