import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from "react-router-dom";
import { createBrowserHistory } from 'history'
import MainLayout from "js/routes/MainLayout";
import 'scss/settings.scss'

const history = createBrowserHistory();

ReactDOM.render(
  <React.StrictMode>
      <div className="App">
          <Router history={history}>
              <MainLayout/>
          </Router>
      </div>
  </React.StrictMode>,
  document.getElementById('root')
);
