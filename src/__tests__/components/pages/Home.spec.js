import React from "react";
import {render, screen, fireEvent, act} from '@testing-library/react'
import Home from 'js/components/pages/Home'
import {getVideos} from 'js/axios/youtube'
import {waitFor, waitForElementToBeRemoved, within} from "@testing-library/dom";
import {createEvent} from "__tests__/__mocks__/testUtils";

jest.mock('js/axios/youtube')

describe('<Home/>', () => {
    const errorMessage = 'API call failed'
    const keyword = 'NFL';
    const firstPageResult = {
        nextPageToken: "CAoQAA",
        items: [
            {
                etag: "yzPoJL4PKSrJ4zAUrPlZMJH9zmQ",
                info: {
                    videoId: "10MjAQKz6cQ",
                    title: "Saints vs. Eagles Week 14 Highlights | NFL 2020",
                    description: "The New Orleans Saints take on the Philadelphia Eagles during Week 14 of the 2020 NFL season.",
                    thumbnails: "https://i.ytimg.com/vi/10MjAQKz6cQ/mqdefault.jpg",
                    channelTitle: keyword
                }
            },
            {
                etag: "rKA-YVHXbNh2D_GwP369YO65Ibk",
                info: {
                    videoId: "jrvRIJEtVEg",
                    title: "Vikings vs. Buccaneers Week 14 Highlights | NFL 2020",
                    description: "The Minnesota Vikings take on the Tampa Bay Buccaneers during Week 14 of the 2020 NFL season.",
                    thumbnails: "https://i.ytimg.com/vi/jrvRIJEtVEg/mqdefault.jpg",
                    channelTitle: keyword
                }
            }
        ]
    }
    const nextPageResults = {
        nextPageToken: "CBoQAK",
        items: [
            {
                etag: "yzPoJL4PKSrJ4zAUrPlZMJH9zmF",
                info: {
                    videoId: "10TjAQPz6cQ",
                    title: "New Video 1",
                    description: "Some description 1",
                    thumbnails: "https://i.ytimg.com/vi/10MjAQKz6cQ/somelink1.jpg",
                    channelTitle: 'NFL'
                }
            },
            {
                etag: "rKA-YVHXbNh2R_GmP369YO65IbL",
                info: {
                    videoId: "SrvOIPEtVEk",
                    title: "New Video 2",
                    description: "Some description 2",
                    thumbnails: "https://i.ytimg.com/vi/10MjAQKz6cQ/somelink2.jpg",
                    channelTitle: 'NFL'
                }
            }
        ]
    }
    const errorResponse = {
        response: {
            data: {
                error: {
                    message: errorMessage
                }
            }
        }
    }

    const wrap = async () => {
        return render(<Home/>)
    }

    beforeEach(() => {
        getVideos.mockResolvedValue(firstPageResult)
    })

    it('should display helper text when user lands on page', () => {
        wrap()
        screen.getByText('Please search the video you like.')
    })

    it('should display loader when video results are loading', async () => {
        const result1Title = firstPageResult.items[0].info.title;

        await wrap()

        fireEvent.change(screen.queryByLabelText('Search'), createEvent(keyword))

        expect(getVideos).not.toHaveBeenCalled()

        fireEvent.click(screen.getByRole('button', {name: 'Search'}), createEvent())

        expect(getVideos).toHaveBeenCalledWith(keyword)
        expect(screen.queryByLabelText(result1Title)).not.toBeInTheDocument()
        await waitForElementToBeRemoved(() => screen.getByLabelText('Loading'))
        expect(screen.queryByLabelText(result1Title)).toBeInTheDocument()
    })

    it('should display error message when failed to fetch video list', async () => {
        getVideos.mockRejectedValue(errorResponse)
        await wrap()

        fireEvent.change(screen.queryByLabelText('Search'), createEvent(keyword))
        fireEvent.click(screen.getByRole('button', {name: 'Search'}), createEvent())

        expect(getVideos).toHaveBeenCalledWith(keyword)
        await waitForElementToBeRemoved(() => screen.getByLabelText('Loading'))
        expect(screen.queryByText('Please search the video you like.')).not.toBeInTheDocument()
        screen.getByText(errorMessage)
    })

    it('should display list of videos related to the keyword entered', async () => {
        const item1 = firstPageResult.items[0]
        const item2 = firstPageResult.items[1]
        await wrap()

        fireEvent.change(screen.queryByLabelText('Search'), createEvent(keyword))
        fireEvent.click(screen.getByRole('button', {name: 'Search'}), createEvent())

        await waitForElementToBeRemoved(() => screen.getByText('Please search the video you like.'))

        expect(getVideos).toHaveBeenCalledWith(keyword)

        const withInResult1 = within(screen.getByLabelText(item1.info.title));
        expect(withInResult1.getByTestId(item1.info.videoId).src).toBe(item1.info.thumbnails)
        withInResult1.getByText(item1.info.title)
        withInResult1.getByText(item1.info.channelTitle)


        const withInResult2 = within(screen.getByLabelText(item2.info.title));
        expect(withInResult2.getByTestId(item2.info.videoId).src).toBe(item2.info.thumbnails)
        withInResult2.getByText(item2.info.title)
        withInResult2.getByText(item2.info.channelTitle)

    })

    it('should display more results when user scroll to the bottom of the page', async () => {
        getVideos.mockResolvedValueOnce(firstPageResult)
        getVideos.mockResolvedValueOnce(nextPageResults)
        await wrap()

        fireEvent.change(screen.queryByLabelText('Search'), createEvent(keyword))
        fireEvent.click(screen.getByRole('button', {name: 'Search'}), createEvent())

        await waitForElementToBeRemoved(() => screen.getByText('Please search the video you like.'))

        await fireEvent.scroll(window, {target: {scrollY: 2000}})

        await waitFor(() => expect(getVideos.mock.calls[1]).toEqual(['NFL', {pageToken: firstPageResult.nextPageToken}]))

        const expectedResultsList = [...firstPageResult.items, ...nextPageResults.items]
        expectedResultsList.map((item) => {
            const withInItem = within(screen.getByLabelText(item.info.title));

            expect(withInItem.getByTestId(item.info.videoId).src).toBe(item.info.thumbnails)
            withInItem.getByText(item.info.title)
            withInItem.getByText(item.info.channelTitle)
        })
    })
})
