export const createEvent = (value = '') => {
    return {
        preventDefault: () => null,
        target: {value}
    }
}