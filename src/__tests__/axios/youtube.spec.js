import axios from 'axios'
import {getVideos} from "js/axios/youtube";

jest.mock('axios')

describe('youtube', () => {
    describe('#getVideos', () => {
        const axiosResponse = {
            data: {
                kind: "youtube#searchListResponse",
                etag: "eMIlof43tX2kAcTY4AMUD5-weBk",
                nextPageToken: "CAoQAA",
                regionCode: "ZZ",
                pageInfo: {
                    totalResults: 1000000,
                    resultsPerPage: 10
                },
                items: [
                    {
                        kind: "youtube#searchResult",
                        etag: "yzPoJL4PKSrJ4zAUrPlZMJH9zmQ",
                        id: {
                            kind: "youtube#video",
                            videoId: "10MjAQKz6cQ"
                        },
                        snippet: {
                            publishedAt: "2020-12-14T00:44:24Z",
                            channelId: "UCDVYQ4Zhbm3S2dlz7P1GBDg",
                            title: "Saints vs. Eagles Week 14 Highlights | NFL 2020",
                            description: "The New Orleans Saints take on the Philadelphia Eagles during Week 14 of the 2020 NFL season. Subscribe to NFL: http://j.mp/1L0bVBu Check out our other ...",
                            thumbnails: {
                                "default": {
                                    url: "https://i.ytimg.com/vi/10MjAQKz6cQ/default.jpg",
                                    width: 120,
                                    height: 90
                                },
                                medium: {
                                    url: "https://i.ytimg.com/vi/10MjAQKz6cQ/mqdefault.jpg",
                                    width: 320,
                                    height: 180
                                },
                                high: {
                                    url: "https://i.ytimg.com/vi/10MjAQKz6cQ/hqdefault.jpg",
                                    width: 480,
                                    height: 360
                                }
                            },
                            channelTitle: "NFL",
                            liveBroadcastContent: "none",
                            publishTime: "2020-12-14T00:44:24Z"
                        }
                    },
                    {
                        kind: "youtube#searchResult",
                        etag: "rKA-YVHXbNh2D_GwP369YO65Ibk",
                        id: {
                            kind: "youtube#video",
                            videoId: "jrvRIJEtVEg"
                        },
                        snippet: {
                            publishedAt: "2020-12-14T00:15:22Z",
                            channelId: "UCDVYQ4Zhbm3S2dlz7P1GBDg",
                            title: "Vikings vs. Buccaneers Week 14 Highlights | NFL 2020",
                            description: "The Minnesota Vikings take on the Tampa Bay Buccaneers during Week 14 of the 2020 NFL season. Subscribe to NFL: http://j.mp/1L0bVBu Check out our other ...",
                            thumbnails: {
                                "default": {
                                    url: "https://i.ytimg.com/vi/jrvRIJEtVEg/default.jpg",
                                    width: 120,
                                    height: 90
                                },
                                medium: {
                                    url: "https://i.ytimg.com/vi/jrvRIJEtVEg/mqdefault.jpg",
                                    width: 320,
                                    height: 180
                                },
                                high: {
                                    url: "https://i.ytimg.com/vi/jrvRIJEtVEg/hqdefault.jpg",
                                    width: 480,
                                    height: 360
                                }
                            },
                            channelTitle: "NFL",
                            liveBroadcastContent: "none",
                            publishTime: "2020-12-14T00:15:22Z"
                        }
                    },
                    {
                        kind: "youtube#searchResult",
                        etag: "JGyJIgjMCdG99wROlU4EVYmKS90",
                        id: {
                            kind: "youtube#video",
                            videoId: "pi4I06YVyNo"
                        },
                        snippet: {
                            publishedAt: "2020-12-14T00:48:10Z",
                            channelId: "UCDVYQ4Zhbm3S2dlz7P1GBDg",
                            title: "Packers vs. Lions Week 14 Highlights | NFL 2020",
                            description: "The Green Bay Packers take on the Detroit Lions during Week 14 of the 2020 NFL season. Subscribe to NFL: http://j.mp/1L0bVBu Check out our other channels: ...",
                            thumbnails: {
                                "default": {
                                    url: "https://i.ytimg.com/vi/pi4I06YVyNo/default.jpg",
                                    width: 120,
                                    height: 90
                                },
                                medium: {
                                    url: "https://i.ytimg.com/vi/pi4I06YVyNo/mqdefault.jpg",
                                    width: 320,
                                    height: 180
                                },
                                high: {
                                    url: "https://i.ytimg.com/vi/pi4I06YVyNo/hqdefault.jpg",
                                    width: 480,
                                    height: 360
                                }
                            },
                            channelTitle: "NFL",
                            liveBroadcastContent: "none",
                            publishTime: "2020-12-14T00:48:10Z"
                        }
                    }
                ]
            }
        };
        const errorResponse = {
            response: {
                data: {
                    error: {
                        message: 'API Failed'
                    }
                }
            }
        }
        beforeEach(() => {
            axios.get.mockResolvedValue(axiosResponse)
        })

        it('should return results of youtube items list', async () => {
            const result = await getVideos('NFL', {pageToken: 'AK25CV'});

            expect(axios.get).toHaveBeenCalledWith(`http://example-test.com/search`, {
                params: {
                    q: 'NFL',
                    part: 'snippet',
                    maxResults: 10,
                    pageToken: 'AK25CV',
                    key: 'ABCDEFGH'
                }
            })
            expect(result).toEqual({
                nextPageToken: "CAoQAA",
                items: [{
                    etag: "yzPoJL4PKSrJ4zAUrPlZMJH9zmQ",
                    info: {
                        videoId: "10MjAQKz6cQ",
                        title: "Saints vs. Eagles Week 14 Highlights | NFL 2020",
                        description: "The New Orleans Saints take on the Philadelphia Eagles during Week 14 of the 2020 NFL season. Subscribe to NFL: http://j.mp/1L0bVBu Check out our other ...",
                        thumbnail: "https://i.ytimg.com/vi/10MjAQKz6cQ/mqdefault.jpg",
                        channelTitle: "NFL"
                    }
                },
                    {
                        etag: "rKA-YVHXbNh2D_GwP369YO65Ibk",
                        info: {
                            videoId: "jrvRIJEtVEg",
                            title: "Vikings vs. Buccaneers Week 14 Highlights | NFL 2020",
                            description: "The Minnesota Vikings take on the Tampa Bay Buccaneers during Week 14 of the 2020 NFL season. Subscribe to NFL: http://j.mp/1L0bVBu Check out our other ...",
                            thumbnail: "https://i.ytimg.com/vi/jrvRIJEtVEg/mqdefault.jpg",
                            channelTitle: "NFL"
                        }
                    },
                    {
                        etag: "JGyJIgjMCdG99wROlU4EVYmKS90",
                        info: {
                            videoId: "pi4I06YVyNo",
                            title: "Packers vs. Lions Week 14 Highlights | NFL 2020",
                            description: "The Green Bay Packers take on the Detroit Lions during Week 14 of the 2020 NFL season. Subscribe to NFL: http://j.mp/1L0bVBu Check out our other channels: ...",
                            thumbnail: "https://i.ytimg.com/vi/pi4I06YVyNo/mqdefault.jpg",
                            channelTitle: "NFL"
                        }
                    }]
            })
        })

        it('should return error when youtube query failed', async () => {
            expect.assertions(1);
            axios.get.mockRejectedValue(errorResponse)

            try {
                await getVideos('NFL', {pageToken: 'AK25CV'})
            } catch (e) {
                expect(e).toEqual(errorResponse)
            }
        })
    })
})