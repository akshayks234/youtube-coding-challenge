import axios from 'axios'

const BASE_API = process.env.REACT_APP_YOUTUBE_BASE_API

const transformResult = (data) => {
    return {
        nextPageToken: data.nextPageToken,
        items: data.items.map((item) => {
            return {
                etag: item.etag,
                info: {
                    videoId: item.id.videoId,
                    title: item.snippet.title,
                    description: item.snippet.description,
                    thumbnail: item.snippet.thumbnails.medium.url,
                    channelTitle: item.snippet.channelTitle
                }
            }
        })
    }
}

const getVideos = async (keyword, options = {}) => {
    const DEFAULT_RESULT_COUNT = 10;
    const INFO_SET = 'snippet';

    const finalOptions = {
        params: {
            q: keyword,
            part: INFO_SET,
            maxResults: DEFAULT_RESULT_COUNT,
            ...options,
            key: process.env.REACT_APP_YOUTUBE_API_KEY
        }
    }

    const res = await axios.get(`${BASE_API}/search`, finalOptions)
    return transformResult(res.data)
}

export {getVideos}