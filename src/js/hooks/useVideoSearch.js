import {useCallback, useState} from "react";
import {getVideos} from "js/axios/youtube";

const useVideoSearch = () => {
    const [searchTerm, setSearchTerm] = useState('')
    const [results, setResults] = useState({nextPageToken: undefined, items: []})
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)

    const handleScroll = useCallback(async () => {
        if (loading) return

        setLoading(true)
        try {
            const response = await getVideos(searchTerm, {pageToken: results.nextPageToken});
            setResults(existingResults => ({
                nextPageToken: response.nextPageToken,
                items: [...existingResults.items, ...response.items]
            }))
            setError('')
        } catch (err) {
            const {data} = err.response
            setError(data.error.message)
        }
        setLoading(false)
    }, [ results, loading, searchTerm ])

    const fetchVideos = useCallback(async (value) => {
        setLoading(true)
        try {
            const response = await getVideos(value);
            setResults(response)
            setSearchTerm(value)
            setError('')
        } catch (err) {
            const {data} = err.response
            setError(data.error.message)
        }
        setLoading(false)
    }, [])

    return {
        handleScroll,
        fetchVideos,
        error,
        results,
        loading
    }
}

export default useVideoSearch