import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {INDEX} from "js/routes/PageUrls"
import Home from 'js/components/pages/Home'
import BaseLayout from "js/routes/BaseLayout";


const MainLayout = () => (
	<Switch>
		<Route
			exact
			path={INDEX}
		>
			<BaseLayout>
				<Home/>
			</BaseLayout>
		</Route>
	</Switch>
);

export default MainLayout;
