import React from 'react';
import PropTypes from 'prop-types';
import Header from "js/components/shared/Header"
import Footer from "js/components/shared/Footer"

const BaseLayout = ({children, ...restProps}) => {
	return <>
		<Header className='site-header'/>
		<main className="main-container">
			{React.cloneElement(children, restProps)}
		</main>
		<Footer className='site-footer'/>
	</>
};

BaseLayout.propTypes = {
	children: PropTypes.node
};

export default BaseLayout;
