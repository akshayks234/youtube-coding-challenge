import React from "react";
import 'scss/video-card.scss'
import {shape, string} from "prop-types";

const VideoCard = ({item}) => {
    const { info } = item

    return <li aria-label={info.title} className="video-card">
        <div className="thumbnail-container">
            <a href={`https://www.youtube.com/watch?v=${info.videoId}`}>
                <img data-testid={info.videoId} className="thumbnail" src={info.thumbnail} alt=""/>
            </a>
        </div>
        <div className="details">
            <h4 className="title">{info.title}</h4>
            <span className="channel-title">{info.channelTitle}</span>
        </div>
    </li>
}

VideoCard.propTypes = {
    info: shape({
        info: shape({
            title: string,
            videoId: string,
            thumbnail: string,
            channelTitle: string
        }).isRequired
    })
};

export default VideoCard