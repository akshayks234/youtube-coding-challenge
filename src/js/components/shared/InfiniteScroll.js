import {useCallback, useEffect} from "react";
import {node, func} from "prop-types";

const InfiniteScroll = ({ children, onNext }) => {
    const handleScroll = useCallback(() => {
        const { clientHeight, scrollTop, scrollHeight} = document.querySelector('html');
        if(clientHeight + scrollTop === scrollHeight) onNext()
    }, [onNext])

    useEffect(() => {
        window.addEventListener('scroll', handleScroll, false);
        return () => window.removeEventListener('scroll', handleScroll, false)
    }, [handleScroll])

    return children
}

InfiniteScroll.propTypes = {
    onNext: func.isRequired,
    children: node.isRequired
}


export default InfiniteScroll