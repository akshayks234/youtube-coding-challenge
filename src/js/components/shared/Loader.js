import React from "react";
import 'scss/loader.scss'

const Loader = () => {
    return <div aria-label="Loading" className="page-loader"/>
}

export default Loader