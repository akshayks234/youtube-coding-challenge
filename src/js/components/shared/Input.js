import React from "react";
import { string } from 'prop-types'

const Input = ({className, ...restProps}) => {
    return <input {...restProps} className={`form-input ${className}`}/>
}

Input.propTypes = {
    className: string
}

Input.defaultProps = {
    className: ''
}


export default Input