import React from "react";
import {array} from "prop-types";
import VideoCard from "js/components/shared/VideoCard";
import 'scss/video-list.scss'

const VideoList = ({ list }) => {
    return <ul className="video-list">
        {
            list.map((item, key) => (
                <VideoCard
                    key={key}
                    item={item}
                />
            ))
        }
    </ul>
}

VideoList.propTypes = {
    list: array
};

VideoList.defaultProps = {
    list: []
};

export default VideoList