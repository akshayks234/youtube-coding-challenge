import React from "react";
import { string } from 'prop-types'

const Button = ({text, className, ...restProps}) => {
    return <button className={`app-btn ${className}`} {...restProps}>{text}</button>
}

Button.propTypes = {
    text: string.isRequired,
    className: string
}

Button.defaultProps = {
    className: ''
}

export default Button