import React from "react";
import {string} from "prop-types";
import 'scss/footer.scss'

const Footer = ({ className }) => {
    return <footer className={className}>Footer Content</footer>
}

Footer.propTypes = {
    className: string.isRequired
}

export default Footer