import React from "react";
import {string} from "prop-types";
import 'scss/header.scss'

const Header = ({ className }) => {
    return <header className={className}>Youtube Search</header>
}

Header.propTypes = {
    className: string.isRequired
}


export default Header