import React, {useState} from "react";
import {func} from 'prop-types'
import Input from "js/components/shared/Input";
import Button from "js/components/shared/Button";
import 'scss/search-bar.scss'

const SearchBar = ({onSearch}) => {
    let [value, setValue] = useState('');

    const handleChange = (e) => {
        setValue(e.target.value)
    }

    const handleSearch = (e) => {
        e.preventDefault()
        onSearch(value)
    }

    return <form className="app-form search-bar" onSubmit={handleSearch}>
        <Input aria-label="Search"
               value={value}
               onChange={handleChange}
               placeholder="Search"
               id="search" type="text"/>
        <Button text="Search" className="search-btn" onClick={handleSearch}/>
    </form>

}

SearchBar.propTypes = {
    onSearch: func.isRequired,
}

export default SearchBar