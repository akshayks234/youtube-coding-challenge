import React from "react";
import VideoList from "js/components/shared/VideoList";
import SearchBar from "js/components/shared/SearchBar";
import Loader from "js/components/shared/Loader";
import InfiniteScroll from "js/components/shared/InfiniteScroll";
import useVideoSearch from "js/hooks/useVideoSearch";
import 'scss/home.scss'

const Home = () => {
    const {
        handleScroll,
        fetchVideos,
        results,
        error,
        loading
    } = useVideoSearch()

    return <div className="home-page">
        <SearchBar onSearch={fetchVideos}/>
        {results.items.length === 0 && !error && <p>Please search the video you like.</p>}
        {
            error ? <span className="error-message">{error}</span> :
                (<InfiniteScroll onNext={handleScroll}>
                    <VideoList list={results.items}/>
                </InfiniteScroll>)

        }
        {loading && <Loader/>}
    </div>
}

export default Home